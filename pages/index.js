import Image from "next/image";
import { Inter } from "next/font/google";
import { useEffect, useState } from "react";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
    const [dataInput, setDataInput] = useState("");
    const [state, setState] = useState([]);
    const [started, setStarted] = useState(false);
    const [selectAlgo, setSelectAlgo] = useState("bubble-sort");
    const [isStop, setIsStop] = useState(false);
    const [time, setTime] = useState(1000);

    const startSort = () => {
        setStarted(true);
        let arr = [...state];

        let i = 0;
        let j = 0;

        const bubbleSort = () => {
            if (i < state.length - 1) {
                if (j < state.length - 1 - i) {
                    arr[j].isComparing = true;
                    arr[j + 1].isComparing = true;
                    setState([...arr]);

                    setTimeout(() => {
                        if (arr[j].value > arr[j + 1].value) {
                            // Swap elements
                            let temp = arr[j];
                            arr[j] = arr[j + 1];
                            arr[j + 1] = temp;

                            setTimeout(() => {
                                arr[j].isSwapping = true;
                                arr[j + 1].isSwapping = true;
                                setState([...arr]);

                                setTimeout(() => {
                                    // Revert background color to red after swapping
                                    arr[j].isSwapping = false;
                                    arr[j + 1].isSwapping = false;
                                    arr[j].bg = "red";
                                    arr[j + 1].bg = "red";
                                    arr[j].isComparing = false;
                                    arr[j + 1].isComparing = false;
                                    setState([...arr]);
                                    bubbleSort();
                                }, time); // Delay of 500 milliseconds (0.5 second)

                                // Change background color to yellow during swapping
                            }, time); // Delay of 1000 milliseconds (1 second)
                        } else {
                            setTimeout(() => {
                                arr[j].isComparing = false;
                                arr[j + 1].isComparing = false;
                                setState([...arr]);
                                j++;
                                bubbleSort();
                            }, time); // Delay of 500 milliseconds (0.5 second)
                        }
                    }, time); // Delay of 1000 milliseconds (1 second)
                } else {
                    arr[j].bg = "green";
                    setState([...arr]);
                    i++;
                    j = 0;
                    bubbleSort();
                }
            } else {
                arr.forEach((ele) => {
                    ele.bg = "green";
                });
                setState([...arr]);
                setStarted(false);
            }
        };

        //     if (i < state.length - 1) {
        //         let minIndex = i;

        //         for (let k = i + 1; k < arr.length; k++) {
        //             if (arr[k].value < arr[minIndex].value) {
        //                 minIndex = k;
        //             }
        //         }

        //         if (minIndex !== i) {
        //             // arr[minIndex].isComparing = true;
        //             // arr[i].isComparing = true;
        //             // setState([...arr]);

        //             setTimeout(() => {
        //                 // Swap elements
        //                 let temp = arr[i];
        //                 arr[i] = arr[minIndex];
        //                 arr[minIndex] = temp;

        //                 arr[i].isSwapping = true;
        //                 arr[minIndex].isSwapping = true;
        //                 setState([...arr]);

        //                 setTimeout(() => {
        //                     // Revert background color to red after swapping
        //                     arr[i].isSwapping = false;
        //                     arr[minIndex].isSwapping = false;
        //                     arr[i].bg = "red";
        //                     arr[minIndex].bg = "red";
        //                     arr[i].isComparing = false;

        //                     setState([...arr]);

        //                     setTimeout(() => {
        //                         i++;
        //                         selectionSort();
        //                     }, 500); // Delay of 500 milliseconds (0.5 second)
        //                 }, 800); // Delay of 800 milliseconds (0.8 second)
        //             }, 800); // Delay of 800 milliseconds (0.8 second)
        //         } else {
        //             // arr[minIndex].bg = "green";
        //             // setState([...arr]);
        //             i++;
        //             selectionSort();
        //         }
        //     } else {
        //         arr.forEach((ele) => {
        //             ele.bg = "green";
        //         });
        //         setState([...arr]);
        //         setStarted(false);
        //     }
        // };
        const selectionSort = () => {
            if (i < arr.length - 1) {
                let minIndex = i;
                let loopIndex = i + 1; // Variable to keep track of the loop index

                const loopStep = () => {
                    if (loopIndex < arr.length) {
                        arr[loopIndex].isComparing = true;
                        arr[minIndex].isComparing = true;
                        arr[loopIndex].bg = "black"; // Change background color for each step of the loop
                        arr[minIndex].bg = "black"; // Change background color for each step of the loop
                        arr[i].bg = "yellow";
                        setState([...arr]);

                        setTimeout(() => {
                            if (arr[loopIndex].value < arr[minIndex].value) {
                                arr[minIndex].isComparing = false;
                                arr[minIndex].bg = "red";
                                minIndex = loopIndex;
                                arr[minIndex].isComparing = true;
                                arr[minIndex].bg = "black"; // Change background color for the new minimum index
                            } else {
                                arr[loopIndex].isComparing = false;
                                arr[loopIndex].bg = "red";
                            }
                            setState([...arr]);
                            loopIndex++;
                            setTimeout(loopStep, time); // Perform the next step of the loop
                        }, time); // Delay of 500 milliseconds (0.5 second)
                    } else {
                        // Loop ends, swap elements
                        if (minIndex !== i) {
                            let temp = arr[i];
                            arr[i] = arr[minIndex];
                            arr[minIndex] = temp;

                            arr[i].isSwapping = true;
                            arr[minIndex].isSwapping = true;
                            setState([...arr]);

                            setTimeout(() => {
                                arr[i].isSwapping = false;
                                arr[minIndex].isSwapping = false;
                                arr[i].bg = "green";
                                arr[minIndex].bg = "red";

                                arr[i].isComparing = false;
                                arr[minIndex].isComparing = false;
                                setState([...arr]);

                                setTimeout(() => {
                                    i++;
                                    selectionSort();
                                }, time); // Delay of 500 milliseconds (0.5 second)
                            }, time); // Delay of 800 milliseconds (0.8 second)
                        } else {
                            // No swapping, move to the next iteration

                            arr[minIndex].isSwapping = true;
                            setState([...arr]);
                            setTimeout(() => {
                                arr[minIndex].bg = "green";
                                arr[minIndex].isComparing = false;
                                setState([...arr]);
                                i++;
                                selectionSort();
                            }, time);
                        }
                    }
                };

                loopStep(); // Start the loop
            } else {
                arr.forEach((ele) => {
                    ele.bg = "green";
                    ele.isComparing = false;
                });
                setState([...arr]);
                setStarted(false);
            }
        };

        if (selectAlgo == "bubble-sort") bubbleSort();
        else if (selectAlgo == "selection-sort") selectionSort();
    };

    const changeHandeler = (e) => {
        setDataInput(e.target.value);
    };
    const submitData = (e) => {
        if (/[^\d,]/.test(dataInput)) {
            alert(" please give proper input", () => setDataInput(""));
        } else {
            const arr = dataInput.split(",").map((value) => ({
                value: parseInt(value),
                isSwapping: false,
                isComparing: false,
                bg: "red",
            }));
            setState([...arr]);
        }
    };

    return (
        <div className="p-6">
            <div>
                <input
                    placeholder="1,2,3,4,...."
                    type="text"
                    className=" border-2 rounded-sm p-[6px] outline-none "
                    onChange={changeHandeler}
                />
                <button
                    onClick={submitData}
                    className="bg-teal-400 p-2 px-6 rounded-sm hover:bg-teal-600 transition-all"
                >
                    Enter the data
                </button>
            </div>
            <div className=" space-x-4 ">
                <select
                    onChange={(e) => setSelectAlgo(e.target.value)}
                    className=" border-2 mt-4 p-2 "
                >
                    <option value="bubble-sort">bubble sort</option>
                    <option value="selection-sort">Selection Sort</option>
                </select>
                <input
                    className=" border-2 mt-4 p-2 max-w-fit "
                    type="number"
                    id="time"
                    name="time"
                    value={time}
                    onChange={(e) => setTime(e.target.value)}
                />{" "}
                <label htmlFor="time">Set animation time</label>
            </div>

            <button
                disabled={started}
                onClick={startSort}
                className={`   bg-teal-400 mt-6 p-2 px-6 rounded-sm hover:bg-teal-600 transition-all`}
            >
                Start
            </button>
            <div className="p-8 space-x-4 flex flex-row ">
                {state?.map((ele, ind) => (
                    <div
                        className={`transition-all  relative duration-500 ${
                            ele.isSwapping ? "swap-animation" : ""
                        } ${ele.isComparing ? "compare-animation" : ""}`}
                        style={{
                            height: `${parseInt(ele.value) * 5}px`,
                            width: "5rem",
                            backgroundColor: ele.bg,
                        }}
                        key={ind}
                    >
                        <span className=" !text-black absolute top-[-1.5rem] left-[50%] ">
                            {" "}
                            {ele.value}
                        </span>
                    </div>
                ))}
            </div>
        </div>
    );
}
